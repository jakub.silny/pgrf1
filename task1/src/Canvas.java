import model.Line;
import model.Point;
import rasterize.*;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * trida pro kresleni na platno: zobrazeni pixelu
 *
 * @author PGRF FIM UHK
 * @version 2020
 */

public class Canvas {

	//Vytvoření globalních proměných
	private JPanel panel;
	private Color color;
	private RasterBufferedImage raster;

	//Rasterizery
	private LineRasterizer filledLR;
	private LineRasterizer dashedLR;
	private PolygonRasterizer polygonR;
	//Polygon a List úseček
	private model.Polygon polygon;
	private List<Line> lines;
	private boolean fillPolygon;


	//Pomocné body
	private model.Point point1,point2,point3;

	/*
	Modifikátor identifikuje různé operace
	mod = 0 - Zadna operace
	mod = 1 - Úprava polygonu
	mod = 2 - Kresba úsečky
	mod = 3 - Kresba polygonu
	 */
	private byte mod;

	/*
		slouží k určení pořadí operací
	 */
	private byte mousePressed;

	public Canvas(int width, int height) {
		JFrame frame = new JFrame();
		fillPolygon = false;

		frame.setLayout(new BorderLayout());

		frame.setTitle("UHK FIM PGRF : Jakub Silný :" + this.getClass().getName());
		frame.setResizable(true);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


		//Prvotní inicializace globalních proměných
		color = new Color(0xF6F5F5);
		raster = new RasterBufferedImage(width, height);
		filledLR = new FilledLineRasterizer(raster);
		dashedLR = new DashedLineRasterizer(raster);
		polygonR = new PolygonRasterizer(raster);
		polygon = new model.Polygon(color.getRGB());
		lines = new ArrayList<>();
		point1 = new model.Point(0,0);
		point2 = new model.Point(0,0);
		point3 = new model.Point(0,0);
		mod = 0;
		mousePressed = 0;

		//Nastavení okna
		panel = new JPanel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				present(g);
			}
		};

		panel.setPreferredSize(new Dimension(width, height));
		frame.add(panel, BorderLayout.CENTER);
		frame.pack();
		frame.setVisible(true);

		/*
			---MouseListener---
			-Stisknutí tlačítka
				BUTTON1 - Levé tlačítko myši
					mod1
						Načte pozici myši

					mod2
						Načte první bod
						nebo
						Načte druhý bod a uloží Line do listu lines

				BUTTON3 - Pravé tlačítko myši
					Ukončí momentálně rozpracovanou akci
					Změní mod = 0(Zadna operace - Default)
					Resetuje body

			-Puštění tlačítka
					mod1
						Nahradi editovany bod novym vrcholem
					mod3
						Prida novy bod do polygonu
		 */

		panel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.getButton() == MouseEvent.BUTTON1) {
					if (mod == 1 && mousePressed == 0 && polygon.polygonSize()>0) {
						point3.x = e.getX();
						point3.y = e.getY();
						mousePressed++;
					}

					if (mod == 2) {

						if (mousePressed == 0) {
							point1.x = e.getX();
							point1.y = e.getY();
							mousePressed++;

						} else if (mousePressed == 2) {
							point2.x = e.getX();
							point2.y = e.getY();
							lines.add(new Line(point1, point2, color.getRGB()));

							mousePressed = 0;

						}

					}
					if (mod == 4 && mousePressed == 0) {

						fillPolygon = true;
					}
				}
				if (e.getButton() == MouseEvent.BUTTON3){
					System.out.println("0");
					mod = 0;
					pointReset();
				}
				draw();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (mod == 1 && mousePressed == 2) {
					polygon.setPoint(polygon.closestPoint(point3), point2);
					pointReset();
				}
				if (mod == 3) {
					polygon.addPoint(new model.Point(e.getX(), e.getY()));
				}
				draw();
			}
		});

		/*
			---MouseMotionListener---
			-Pohyb myši
				mod2
					Vykresluje obraz možné úsečky v reálném čase

			-Pohyb myši při stisknutém tlačítku
				mod1
					vykresluje čárkované čáry k novému bodu
				mod3
					vykresluje čárkované čáry k novému bodu
		 */

		panel.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				if(mod == 2) {
					if (mousePressed == 1 || mousePressed == 2) {
						point2.x = e.getX();
						point2.y = e.getY();
						mousePressed = 2;
					}
				}
				draw();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				draw();
				if (mod == 1 && (mousePressed == 1 || mousePressed == 2)) {
					point2.x = e.getX();
					point2.y = e.getY();
					int i = (polygon.closestPoint(point3));

					if(i-1 == -1){
						point1 = polygon.getLastPoint();
					} else {
						point1 = polygon.getPoint(i-1);}
					dashedLR.rasterize(new Line(point1, point2, color),color);

					if(i+1 == polygon.polygonSize()){
						point1 = polygon.getPoint(0);
					} else{
						point1 = polygon.getPoint(i+1);
					}
					dashedLR.rasterize(new Line(point2, point1, color),color);

					mousePressed = 2;

				}
				if (mod == 3 && polygon.polygonSize() > 0) {
					point2.x = e.getX();
					point2.y = e.getY();
					dashedLR.rasterize(new Line(polygon.getLastPoint(), point2, color),color);
					dashedLR.rasterize(new Line(point2, polygon.getPoint(0), color),color);
				}

			}
		});

		/*
			---KeyPressed---
			Klávesy:
			C = Clear
			1, NUMPAD1 = MOD1
			2, NUMPAD2 = MOD2
			3, NUMPAD3 = MOD3
			4, NUMPAD4 = MOD4
		 */
		panel.requestFocus();
		panel.requestFocusInWindow();
		panel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {

				switch (e.getKeyCode()) {
					case KeyEvent.VK_C:
						polygon = new model.Polygon(color.getRGB());
						lines = new ArrayList<>();
						raster.clear();
						break;
					case KeyEvent.VK_1:
					case KeyEvent.VK_NUMPAD1:
						pointReset();
						mod = 1;
						break;
					case KeyEvent.VK_2:
					case KeyEvent.VK_NUMPAD2:
						pointReset();
						mod = 2;
						break;
					case KeyEvent.VK_3:
					case KeyEvent.VK_NUMPAD3:
						pointReset();
						mod = 3;
						break;
					case KeyEvent.VK_4:
					case KeyEvent.VK_NUMPAD4:

						pointReset();
						mod = 4;
						System.out.println("Mod 4");
						break;
				}
				draw();
			}
		});

		//Resize okna
		panel.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				if (panel.getWidth()<1 || panel.getHeight()<1)
					return;
				if (panel.getWidth()<=raster.getWidth() && panel.getHeight()<=raster.getHeight()) //no resize if new one is smaller
					return;
				RasterBufferedImage newRaster = new RasterBufferedImage(panel.getWidth(), panel.getHeight());

				newRaster.draw(raster);
				raster = newRaster;
				filledLR = new FilledLineRasterizer(raster);

			}
		});

	}

	/*
	Přebarvý celé plátno

	Vykreslí úsečky a polygo
	Vykreslí text

	Zobrazí
	 */
	private void draw(){
		clear(0x000000);

		drawLines();
		if (fillPolygon == false) {
			polygonR.rasterize(polygon);

		}

		raster.getGraphics().drawString("Levé tlačítko myši pro provedení akce / Pravé tlačítko myši pro zrušení akce", 5, 15);
		raster.getGraphics().drawString("Stiskněte klávesy:", 5, 30);
		raster.getGraphics().drawString("1 - Volný pohyb myši / Úpravu vrcholů", 5, 45);
		raster.getGraphics().drawString("2 - Kresbu čár", 5, 60);
		raster.getGraphics().drawString("3 - kresbu polygonu", 5, 75);
		raster.getGraphics().drawString("4 - Vyplnění polygonu", 5,120);
		raster.getGraphics().drawString("C - Smazání obrazovky", 5, 90);

		panel.repaint();

	}
	private void drawLines(){
		if(mod == 2){
			if (mousePressed == 2) {
				dashedLR.rasterize(point1.x,point1.y,point2.x, point2.y, color);
			}

		}
		if(lines.size()>0){
			for (Line line : lines) {
				filledLR.rasterize(line,color);
			}
		}
	}

	private void pointReset(){
		point3 = new model.Point(0,0);
		point1 = new model.Point(0,0);
		point2= new Point(0,0);
		mousePressed = 0;
	}

	public void clear(int color) {
		raster.setClearColor(color);
		raster.clear();

	}

	public void present(Graphics graphics) {
		raster.repaint(graphics);
	}

	public void start() {
		clear(0x000000);

		raster.getGraphics().drawString("Levé tlačítko myši pro provedení akce / Pravé tlačítko myši pro zrušení akce", 5, 15);
		raster.getGraphics().drawString("Stiskněte klávesy:", 5, 30);
		raster.getGraphics().drawString("1 - Volný pohyb myši / Úpravu vrcholů", 5, 45);
		raster.getGraphics().drawString("2 - Kresbu čár", 5, 60);
		raster.getGraphics().drawString("3 - kresbu polygonu", 5, 75);
		raster.getGraphics().drawString("4 - Vyplnění polygonu", 5,120);
		raster.getGraphics().drawString("C - Smazání obrazovky", 5, 90);

		panel.repaint();
	}


	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new Canvas(800, 600).start());
	}
}

