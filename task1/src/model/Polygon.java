package model;

import java.util.ArrayList;
import java.util.List;

public class Polygon {

    private List<Point> points;
    private int color;

    public Polygon(int color) {
        points=new ArrayList<>() {
        };
        this.color = color;
    }



    public Point getPoint(int index){
        return points.get(index);
    }

    public void setPoint(int index, Point point){
        points.set(index, point);
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void addPoint(Point point){
        points.add(point);
    }

    //Najde nejbližší bod v poligonu
    public int closestPoint(Point point) {
        int i,index,x2,y2,x1,y1,d,h;
        d = Integer.MAX_VALUE;
        x1=point.x;
        y1=point.y;
        index = Integer.MAX_VALUE;
        for (i = 0; i < points.size(); i++) {
            x2 = getPoint(i).x;
            y2 = getPoint(i).y;
            h = (int)Math.sqrt(Math.pow(Math.abs(x2-x1), 2) + Math.pow(Math.abs(y2-y1), 2));//Výpočet pro zjištění zdálenosti mezi body

            if(d>h){
                d=h;
                index = i;
            }
        }
        return index;
    }

    public int polygonSize(){
        return points.size();
    }

    public Point getLastPoint(){
        return points.get(points.size()-1);
    }


    public List<Point> getPoints() {
        return points;
    }





}
