package model;

import java.awt.*;
public class Seed {
    public Point seed;
    public Color color;
    public int index;


    public Seed(Point seed, Color color, int index) {
        this.seed = seed;
        this.color = color;
        this.index = index;
    }
}
