package rasterize;

import java.awt.*;

public class DashedLineRasterizer extends LineRasterizer{
    public DashedLineRasterizer(Raster raster) {
        super(raster);
    }

    //Dashed line rasterizer pro čárkovanou čáru. Funguje na principu Rozdělení úsečky na n segmentů(idealne liche cislo) a následně počítání sudých a lichých segmenů.
    //Chyby: Při malé velikosti úsečky je dělení nepřesné a úsečka nekončí vždy vykreslovaným segmentem.

    @Override
    public void drawLine(int x1, int y1, int x2, int y2, Color color){

        int x,y,p,k1,k2,t,dx,dy,i;
        float dMax;

        dx=x2-x1;
        dy=y2-y1;

        dMax = Math.max(Math.abs(dx),Math.abs(dy));
        dMax = dMax/13;

        if (Math.abs(dy) < Math.abs(dx)){

            if (x2 < x1) {
                t=x1;
                x1=x2;
                x2=t;

                t=y1;
                y1=y2;
                y2=t;
            }

            x = x1;
            y = y1;
            dx=x2-x1;
            dy=y2-y1;
            i=1;

            if(dy<0){
                i=-1;
                dy=-dy;
            }

            p = 2 * dy - dx;

            k1 = 2 * dy;
            k2 = 2 * (dy - dx);

            raster.setPixel(x, y, color.getRGB());

            while(x<x2) {
                x = x + 1;
                if (p < 0) {
                    p = p + k1;
                } else {
                    p = p + k2;
                    y = y + i;
                }

                if(((x-x1)/((int)dMax+1))%2==0) {
                    raster.setPixel(x, y, color.getRGB());
                }




            }

        } else {
            if (y2 < y1) {
                t=x1;
                x1=x2;
                x2=t;

                t=y1;
                y1=y2;
                y2=t;
            }

            x = x1;
            y = y1;
            dx=x2-x1;
            dy=y2-y1;
            i=1;

            if(dx<0){
                i=-1;
                dx=-dx;
            }

            p = 2 * dx - dy;

            k1 = 2 * dx;
            k2 = 2 * (dx - dy);

            raster.setPixel(x, y, color.getRGB());

            while(y<y2){
                y = y+1;

                if (p<0){
                    p=p+k1;
                }
                else{
                    p=p+k2;
                    x=x+i;
                }
                if(((y-y1)/((int)dMax+1))%2==0) {
                    raster.setPixel(x, y, color.getRGB());
                }

            }
        }
    }
}
