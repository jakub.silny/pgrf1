package rasterize;

import java.util.Optional;

public interface Raster {


    /**
     * Clear canvas
     */
    void clear();

    /**
     * Set clear color
     *
     * @param color clear color
     */
    void setClearColor(int color);

    /**
     * Get Number of rows in this raster
     *
     * @return width
     */
    int getWidth();

    /**
     * Get Number of columns in this raster
     *
     * @return height
     */
    int getHeight();

    /**
     * Get pixel color at [x,y] position
     *
     * @param x horizontal coordinate
     * @param y vertical coordinate
     * @return pixel color
     */
    int getPixel(int x, int y);

    /**
     * Set pixel color at [x,y] position
     *
     * @param x     horizontal coordinate
     * @param y     vertical coordinate
     * @param color pixel color
     */
    void setPixel(int x, int y, int color);
}

    /**
     *
     * @param c column address
     * @param r row address
     * @param Color new color value
     * @return true if provided address was valid; false otherwise
     */

    /**
     *
     * @param c column address
     * @param r row address
     * @return Optional of the pixel value if the provided address was valid; empty Optional otherwise
     */
