package rasterize;

import model.Line;
import model.Point;
import model.Polygon;

import java.awt.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PolygonRasterizer {
    Raster raster;
    Color color;
    FilledLineRasterizer filledLR;

    public PolygonRasterizer(Raster raster) {
        this.raster = raster;
        filledLR = new FilledLineRasterizer(raster);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setColor(int color) {
        this.color = new Color(color);
    }


    // Mezi body vykreslí čáru. na konci spojí poslední bod s prvním
    public void rasterize(Polygon polygon) {
        color = new Color(polygon.getColor());

        for (int i = 0; i < polygon.polygonSize(); i++) {
            if (i + 1 == polygon.polygonSize()) {
                filledLR.rasterize(new Line(polygon.getPoint(i), polygon.getPoint(0), color), color);
            } else {
                filledLR.rasterize(new Line(polygon.getPoint(i), polygon.getPoint(i + 1), color), color);
            }
        }
    }
}




